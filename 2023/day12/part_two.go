package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"github.com/samber/lo"
)

func ValidateTwo(input string, expected []int64, upto int) bool {
	damaged := int64(-1)
	var j int

	for i, c := range []rune(input) {
		if i == upto {
			return true
		}
		if c == '#' {
			if damaged < 0 { // Previous was a dot
				damaged = 1
			} else {
				damaged++
			}
			if j >= len(expected) || damaged > expected[j] {
				return false
			}
		} else {
			if damaged < 0 { // Previous was a dot
				continue
			} else {
				if j >= len(expected) {
					return false
				}
				if expected[j] != damaged {
					return false
				}
				damaged = -1
				j++
			}
		}
	}

	//fmt.Println("match", input, expected)

	return true
}

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		var arrangements int64

		line := scanner.Text()

		input, grps, ok := strings.Cut(line, " ")
		if !ok {
			panic("no space in line")
		}

		// 5x all the things
		input = strings.Join(lo.Times(5, func(_ int) string { return input }), "?")
		grps = strings.Join(lo.Times(5, func(_ int) string { return grps }), ",")

		groups := lo.Map(strings.Split(grps, ","), ParseNum)

		indexes := lo.Reduce([]rune(input), func(agg []int, c rune, i int) []int {
			if c == '?' {
				agg = append(agg, i)
			}
			return agg
		}, []int{})

		var damFill func(input string, idx []int)
		damFill = func(input string, idx []int) {
			if len(idx) == 0 {
				if ValidateTwo(input, groups, len(input)) {
					arrangements++
				}
				return
			}

			ss := make([]rune, len(input))
			copy(ss, []rune(input))

			ss[idx[0]] = '#'
			if ValidateTwo(input, groups, idx[0]) {
				damFill(string(ss), idx[1:])
			}

			ss[idx[0]] = '.'
			if ValidateTwo(input, groups, idx[0]) {
				damFill(string(ss), idx[1:])
			}
		}

		damFill(input, indexes)

		fmt.Println("Found", arrangements, "for", input)

		sum += arrangements
	}

	return sum
}
