package main

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
	"golang.org/x/exp/slices"
)

func ParseNum(s string, _ int) int64 {
	n, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic("number not parsed")
	}

	return n
}

func Validate(input string, expected []int64) bool {
	var actual []int64
	var damaged int64
	now := rune(input[0])

	for _, c := range []rune(input) {
		if c == now {
			if c == '#' {
				damaged++
			}
		} else {
			if c == '#' {
				if damaged > 0 {
					actual = append(actual, damaged)
				}
				damaged = 1
			}
			now = c
		}
	}
	if damaged > 0 {
		actual = append(actual, damaged)
	}

	return slices.Equal(expected, actual)
}

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		var arrangements int64

		line := scanner.Text()

		input, grps, ok := strings.Cut(line, " ")
		if !ok {
			panic("no space in line")
		}

		groups := lo.Map(strings.Split(grps, ","), ParseNum)

		indexes := lo.Reduce([]rune(input), func(agg []int, c rune, i int) []int {
			if c == '?' {
				agg = append(agg, i)
			}
			return agg
		}, []int{})

		var damFill func(input string, idx []int)
		damFill = func(input string, idx []int) {
			if len(idx) == 0 {
				if Validate(input, groups) {
					arrangements++
				}
				return
			}

			ss := make([]rune, len(input))
			copy(ss, []rune(input))

			ss[idx[0]] = '#'
			damFill(string(ss), idx[1:])

			ss[idx[0]] = '.'
			damFill(string(ss), idx[1:])
		}

		damFill(input, indexes)

		sum += arrangements
	}

	return sum
}
