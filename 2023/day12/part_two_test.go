package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartTwo(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input:    "???.### 1,1,3",
			expected: 1,
		},
		{
			input:    ".??..??...?##. 1,1,3",
			expected: 16384,
		},
		{
			input:    "?#?#?#?#?#?#?#? 1,3,1,6",
			expected: 1,
		},
		{
			input:    "????.#...#... 4,1,1",
			expected: 16,
		},
		{
			input:    "????.######..#####. 1,6,5",
			expected: 2500,
		},
		{
			input:    "?###???????? 3,2,1",
			expected: 506250,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partTwo(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
