package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input:    "???.### 1,1,3",
			expected: 1,
		},
		{
			input:    ".??..??...?##. 1,1,3",
			expected: 4,
		},
		{
			input:    "?#?#?#?#?#?#?#? 1,3,1,6",
			expected: 1,
		},
		{
			input:    "????.#...#... 4,1,1",
			expected: 1,
		},
		{
			input:    "????.######..#####. 1,6,5",
			expected: 4,
		},
		{
			input:    "?###???????? 3,2,1",
			expected: 10,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partOne(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
