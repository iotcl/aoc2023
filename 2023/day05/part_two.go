package main

import (
	"bufio"
	"io"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var input []int64
	var mapper Mapper

	mappers := make([]Mapper, 0)

	for scanner.Scan() {
		line := scanner.Text()

		if strings.HasPrefix(line, "seeds:") {
			input = lo.Map(strings.Fields(line[6:]), parseNum)

			continue
		}

		if strings.HasSuffix(line, "map:") {
			if len(mapper.source) > 0 {
				// Skip the initial value
				mappers = append(mappers, mapper)
			}

			src, dest, ok := strings.Cut(line, "-to-")
			if !ok {
				panic("map name not parsed")
			}

			mapper = Mapper{
				source:      src,
				destination: dest[:len(dest)-5],
			}

			continue
		}

		rangeMap := lo.Map(strings.Fields(line), parseNum)
		if len(rangeMap) == 3 {
			mapper.ranges = append(mapper.ranges, MapRange{
				dStart: rangeMap[0],
				sStart: rangeMap[1],
				len:    rangeMap[2],
			})

			continue
		}
	}

	mappers = append(mappers, mapper)

	return lo.Reduce(lo.Chunk(input, 2), func(agg int64, seeds []int64, _ int) int64 {
		for j := int64(0); j < seeds[1]; j++ {
			i := j + seeds[0]
			from := "seed"

			for {
				mapper, ok := lo.Find(mappers, func(m Mapper) bool {
					return m.source == from
				})
				if !ok {
					panic("mapper not found")
				}

				rng, ok := lo.Find(mapper.ranges, func(r MapRange) bool {
					return i >= r.sStart && i < (r.sStart+r.len)
				})

				if ok {
					i = rng.dStart + (i - rng.sStart)
				}

				from = mapper.destination
				if from == "location" {
					break
				}
			}

			if agg < 0 || i < agg {
				agg = i
			}
		}

		return agg
	}, -1)
}
