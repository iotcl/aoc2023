package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func parseNums(line string) []int64 {
	return lo.Map(strings.Fields(line), func(s string, ind int) int64 {
		n, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			fmt.Println(ind, line)
			panic("number not parsed")
		}
		return n
	})
}

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()
		_, input, ok := strings.Cut(line, ":")
		if !ok {
			panic("card not found")
		}
		before, after, ok := strings.Cut(input, "|")
		if !ok {
			panic("mine not found")
		}

		winners := parseNums(before)
		mine := parseNums(after)

		win := lo.Reduce(lo.Intersect(winners, mine), func(agg int64, _ int64, _ int) int64 {
			return agg * 2
		}, 1)

		sum += win / 2

	}

	return sum
}
