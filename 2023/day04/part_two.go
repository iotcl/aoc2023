package main

import (
	"bufio"
	"io"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var i int

	copies := []int64{1}

	for scanner.Scan() {
		if i == len(copies) {
			copies = append(copies, 1)
		}

		line := scanner.Text()
		_, input, ok := strings.Cut(line, ":")
		if !ok {
			panic("card not found")
		}
		before, after, ok := strings.Cut(input, "|")
		if !ok {
			panic("mine not found")
		}

		winners := parseNums(before)
		mine := parseNums(after)
		count := len(lo.Intersect(winners, mine))

		for j := 1; j <= count; j++ {
			if i+j < len(copies) {
				copies[i+j] += copies[i]
			} else {
				copies = append(copies, copies[i]+1)
			}
		}

		i++
	}

	return lo.Sum(copies)
}
