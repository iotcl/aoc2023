package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartTwo(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input:    "hello",
			expected: 1,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partTwo(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
