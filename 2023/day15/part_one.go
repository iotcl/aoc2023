package main

import (
	"bufio"
	"io"
)

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum, current int64

	for scanner.Scan() {
		line := scanner.Text()

		for _, c := range []rune(line) {
			if c == ',' {
				sum += current
				current = 0
				continue
			}

			current += int64(c)
			current *= 17
			current = current % 256
		}
		sum += current
	}

	return sum
}
