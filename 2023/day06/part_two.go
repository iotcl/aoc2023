package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	lineToNum := func() int64 {
		if !scanner.Scan() {
			panic("no line found")
		}
		_, numbers, ok := strings.Cut(scanner.Text(), ":")
		if !ok {
			panic("colon not found")
		}
		return lo.Reduce(strings.Fields(numbers), func(agg int64, s string, _ int) int64 {
			n, err := strconv.ParseInt(s, 10, 64)
			if err != nil {
				panic("number not parsed")
			}

			for f := n; f > 0; f = f / 10 {
				agg *= 10
			}

			return agg + n
		}, 0)
	}

	time := lineToNum()
	dist := lineToNum()

	fmt.Println(time, dist)

	var ways int64

	for j := int64(1); j < time; j++ {
		if j*(time-j) > dist {
			ways++
		}
	}

	return ways
}
