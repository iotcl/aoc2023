package main

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	lineToSlice := func() []int64 {
		if !scanner.Scan() {
			panic("no line found")
		}
		_, numbers, ok := strings.Cut(scanner.Text(), ":")
		if !ok {
			panic("colon not found")
		}
		return lo.Map(strings.Fields(numbers), func(s string, _ int) int64 {
			n, err := strconv.ParseInt(s, 10, 64)
			if err != nil {
				panic("number not parsed")
			}

			return n
		})
	}

	times := lineToSlice()
	distances := lineToSlice()

	return lo.Reduce(times, func(agg int64, t int64, i int) int64 {
		var ways int64

		for j := int64(1); j < t; j++ {
			if j*(t-j) > distances[i] {
				ways++
			}
		}

		return agg * ways
	}, 1)
}
