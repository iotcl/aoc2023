package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input: `Time:      7  15   30
Distance:  9  40  200`,
			expected: 288,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partOne(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
