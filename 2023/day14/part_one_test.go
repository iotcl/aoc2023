package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `OOOO.#.O..
OO..#....#
OO..O##..O
O..#.OO...
........#.
..#....#.#
..O..#.O.O
..O.......
#....###..
#....#....`,
			expected: 136,
		},
		{
			input: `O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....`,
			expected: 136,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partOne(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
