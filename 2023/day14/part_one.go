package main

import (
	"bufio"
	"fmt"
	"io"

	"github.com/samber/lo"
)

func partOne(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var rocks [][]rune

	for scanner.Scan() {
		line := scanner.Text()

		rocks = append(rocks, []rune(line))
	}

	_ = func() {
		for _, row := range rocks {
			for _, c := range row {
				fmt.Printf("%c", c)
			}
			fmt.Println()
		}
	}

	for y, row := range rocks {
		for x := range row {
			if rocks[y][x] != 'O' {
				continue
			}

			for yy := y - 1; yy >= 0; yy-- {
				if rocks[yy][x] == '.' {
					rocks[yy][x] = 'O'
					rocks[yy+1][x] = '.'
				} else {
					break
				}
			}
		}
	}

	//printIt()

	height := len(rocks)

	var sum int

	for y, row := range rocks {
		sum += lo.Count(row, 'O') * (height - y)
	}

	return sum
}
