package main

import (
	"bufio"
	"fmt"
	"io"
	"time"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var rocks [][]rune

	for scanner.Scan() {
		line := scanner.Text()

		rocks = append(rocks, []rune(line))
	}

	printIt := func(title string) {
		fmt.Println(title)
		for _, row := range rocks {
			for _, c := range row {
				fmt.Printf("%c", c)
			}
			fmt.Println()
		}
	}

	height := len(rocks)
	width := len(rocks[0])
	start := time.Now()
	free := make([]int, 0, height+width)
	var x, y int

	for cycle := 0; cycle < 1000000000; cycle++ {
		for i := 0; i < width; i++ {
			free := free[:0]
			x = i

			for j := 0; j < height; j++ {
				y = j

				if rocks[y][x] == '.' {
					free = append(free, j)
					continue
				}
				if rocks[y][x] == '#' {
					free = free[:0]
					continue
				}
				if len(free) == 0 {
					continue
				}
				rocks[free[0]][x] = 'O'
				rocks[y][x] = '.'

				free = append(free, j)[1:]
			}
		}
		//printIt("North")

		for i := 0; i < height; i++ {
			free := free[:0]
			y = i

			for j := 0; j < width; j++ {
				x = j

				if rocks[y][x] == '.' {
					free = append(free, j)
					continue
				}
				if rocks[y][x] == '#' {
					free = free[:0]
					continue
				}
				if len(free) == 0 {
					continue
				}
				rocks[y][free[0]] = 'O'
				rocks[y][x] = '.'

				free = append(free, j)[1:]
			}
		}
		//printIt("West")

		for i := 0; i < width; i++ {
			free := free[:0]
			x = i

			for j := height - 1; j >= 0; j-- {
				y = j

				if rocks[y][x] == '.' {
					free = append(free, j)
					continue
				}
				if rocks[y][x] == '#' {
					free = free[:0]
					continue
				}
				if len(free) == 0 {
					continue
				}
				rocks[free[0]][x] = 'O'
				rocks[y][x] = '.'

				free = append(free, j)[1:]
			}
		}
		//printIt("South")

		for i := 0; i < height; i++ {
			free := free[:0]
			y = i

			for j := width - 1; j >= 0; j-- {
				x = j

				if rocks[y][x] == '.' {
					free = append(free, j)
					continue
				}
				if rocks[y][x] == '#' {
					free = free[:0]
					continue
				}
				if len(free) == 0 {
					continue
				}
				rocks[y][free[0]] = 'O'
				rocks[y][x] = '.'

				free = append(free, j)[1:]
			}
		}
		//printIt("East")

		if cycle%100000 == 0 {
			fmt.Println(cycle, "cycles in", time.Since(start))
		}
	}

	printIt("Done")

	var sum int

	for y, row := range rocks {
		sum += lo.Count(row, 'O') * (height - y)
	}

	return sum
}
