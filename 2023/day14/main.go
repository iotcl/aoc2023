package main

import (
	"fmt"
	"os"
)

func main() {
	r := os.Stdin

	fmt.Println("Solution to part one:", partOne(r))

	if _, err := r.Seek(0, 0); err != nil {
		panic("failed to seek")
	}

	fmt.Println("Solution to part two", partTwo(r))
}
