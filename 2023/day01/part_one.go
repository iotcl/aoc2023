package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"

	"github.com/samber/lo"
)

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()

		nums := lo.FilterMap([]rune(line), func(c rune, _ int) (int64, bool) {
			if c < '0' || c > '9' {
				return 0, false
			}

			i, err := strconv.ParseInt(string(c), 10, 64)
			if err != nil {
				panic("Not a number")
			}

			return i, true
		})

		first, err := lo.Nth(nums, 0)
		if err != nil {
			fmt.Println(line)
			panic("no first number")
		}
		last, err := lo.Last(nums)
		if err != nil {
			panic("no last number")
		}

		sum += first*10 + last
	}

	return sum
}
