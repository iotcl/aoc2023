package main

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	numbers := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()

		var nums []int64

		for i := 0; i < len(line); i++ {
			if line[i] >= '0' && line[i] <= '9' {
				v, err := strconv.ParseInt(string(line[i]), 10, 64)
				if err != nil {
					panic("Not a number")
				}

				nums = append(nums, v)

				continue
			}

			for j, name := range numbers {
				if strings.HasPrefix(line[i:], name) {
					nums = append(nums, int64(j+1))
				}
			}
		}

		first, err := lo.Nth(nums, 0)
		if err != nil {
			panic("no first number")
		}
		last, err := lo.Last(nums)
		if err != nil {
			panic("no last number")
		}

		sum += first*10 + last
	}

	return sum
}
