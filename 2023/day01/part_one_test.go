package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input: `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`,
			expected: 142,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partOne(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
