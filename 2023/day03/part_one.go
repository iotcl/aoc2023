package main

import (
	"bufio"
	"io"
	"strconv"

	"github.com/samber/lo"
)

type PartNum struct {
	num       int64
	x1, x2, y int
}

type Part struct {
	symbol rune
	x, y   int
}

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var y int

	var partNums []PartNum
	var parts []Part

	for scanner.Scan() {
		line := scanner.Text()

		var numRunes []rune
		var x int

		parseNum := func() {
			if len(numRunes) == 0 {
				return
			}
			n, err := strconv.ParseInt(string(numRunes), 10, 64)
			if err != nil {
				panic("part number not parsed")
			}

			partNums = append(partNums, PartNum{
				num: n,
				x1:  x - len(numRunes),
				x2:  x - 1,
				y:   y,
			})

			numRunes = numRunes[:0]
		}

		for xa, c := range line {
			x = xa

			if c >= '0' && c <= '9' {
				numRunes = append(numRunes, c)
			} else {
				parseNum()

				if c == '.' || c == ' ' {
					continue
				}

				parts = append(parts, Part{
					symbol: c,
					x:      x,
					y:      y,
				})
			}
		}
		parseNum()

		y++
	}

	sum := lo.Reduce(partNums, func(agg int64, n PartNum, _ int) int64 {
		_, ok := lo.Find(parts, func(p Part) bool {
			if (n.y-p.y) > 1 || (n.y-p.y) < -1 {
				return false
			}
			if (n.x1-p.x) > 1 || (n.x2-p.x) < -1 {
				return false
			}
			return true
		})

		if ok {
			agg += n.num
		}

		return agg
	}, 0)

	return sum
}
