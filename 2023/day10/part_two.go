package main

import (
	"bufio"
	"fmt"
	"io"

	"github.com/samber/lo"
)

var boxDraw = map[rune]rune{
	'-': '─',
	'|': '│',
	'L': '╰',
	'J': '╯',
	'7': '╮',
	'F': '╭',
	'S': '┼',
	'┼': '┼',
}

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var matrix [][]rune

	start := Coord{x: -1}

	for scanner.Scan() {
		line := []rune(scanner.Text())
		matrix = append(matrix, line)

		if x := lo.IndexOf(line, 'S'); x != -1 {
			start.x = x
		}

		if start.x < 0 {
			start.y++
		}
	}

	dirs := lo.Filter(Dir('S'), func(d Coord, _ int) bool {
		next := start.Move(d)

		if next.x < 0 || next.y < 0 {
			return false
		}

		nextDirs := Dir(matrix[next.y][next.x])
		return lo.Some(nextDirs, []Coord{d.Op()})
	})

	if len(dirs) != 2 {
		fmt.Println(dirs)
		panic("wrong number of start directions")
	}

	pos := []Coord{start, start}

	for done := false; !done; {
		for j, d := range dirs {
			next := pos[j].Move(d)
			nextDirs := Dir(matrix[next.y][next.x])

			c, ok := boxDraw[matrix[pos[j].y][pos[j].x]]
			if !ok {
				done = true
				break
			}
			matrix[pos[j].y][pos[j].x] = c

			found := -1
			for k, dd := range nextDirs {
				if !dd.Op().Eq(d) {
					found = k
				}
			}
			if found < 0 {
				continue
			}

			pos[j] = next
			dirs[j] = nextDirs[found]
		}
	}

	for y, row := range matrix {
		for x, c := range row {
			if lo.Contains(lo.Values(boxDraw), c) {
				continue
			}
			matrix[y][x] = '.'
		}
	}

	// Scale up the map 3x
	var scaled [][]rune
	for _, row := range matrix {
		lines := make([][]rune, 3)

		for _, c := range row {
			pixel := [][]rune{
				{' ', ' ', ' '},
				{' ', c, ' '},
				{' ', ' ', ' '},
			}

			k, ok := lo.FindKey(boxDraw, c)
			if ok {
				for _, e := range Dir(k) {
					if e.x != 0 {
						pixel[e.y+1][e.x+1] = boxDraw['-']
					} else {
						pixel[e.y+1][e.x+1] = boxDraw['|']
					}
				}
			}

			for i, subpixel := range pixel {
				lines[i] = append(lines[i], subpixel...)
			}
		}

		for _, l := range lines {
			scaled = append(scaled, l)
		}
	}

	matrix = scaled
	matrix[0][0] = 'O'

	for i := 0; ; i++ {
		var replaced int

		for y, row := range matrix {
			for x, c := range row {
				// Stop at any border
				if c != '.' && c != ' ' {
					continue
				}

				if (x > 0 && matrix[y][x-1] == 'O') ||
					(x < len(row)-1 && matrix[y][x+1] == 'O') ||
					(y > 0 && matrix[y-1][x] == 'O') ||
					(y < len(matrix)-1 && matrix[y+1][x] == 'O') {
					matrix[y][x] = 'O'
					replaced++
				}
			}
		}
		if replaced == 0 {
			fmt.Printf("Stopped replacing dots after [%v] steps\n", i)

			break
		}
	}

	dots := int64(0)

	for _, row := range matrix {
		for _, c := range row {
			if c == '.' {
				dots++
			}
			fmt.Printf("%c", c)
		}
		fmt.Printf("\n")
	}
	return dots
}
