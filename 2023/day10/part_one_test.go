package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input: `-L|F7
7S-7|
L|7||
-L-J|
L|-JF`,
			expected: 4,
		},
		{
			input: `7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ`,
			expected: 8,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partOne(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
