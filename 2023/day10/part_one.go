package main

import (
	"bufio"
	"fmt"
	"io"

	"github.com/samber/lo"
)

type Coord struct {
	x, y int
}

func (c Coord) Move(d Coord) Coord {
	return Coord{
		x: c.x + d.x,
		y: c.y + d.y,
	}
}

func (c Coord) Op() Coord {
	return Coord{
		x: c.x * -1,
		y: c.y * -1,
	}
}

func (c Coord) Eq(other Coord) bool {
	return c.x == other.x && c.y == other.y
}

var North = Coord{y: -1}
var East = Coord{x: 1}
var South = Coord{y: 1}
var West = Coord{x: -1}

func Dir(c rune) []Coord {
	switch c {
	case '|':
		return []Coord{North, South}
	case '-':
		return []Coord{East, West}
	case 'L':
		return []Coord{North, East}
	case 'J':
		return []Coord{North, West}
	case '7':
		return []Coord{South, West}
	case 'F':
		return []Coord{South, East}
	case 'S':
		return []Coord{North, East, South, West}
	}

	return []Coord{}
}

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var matrix [][]rune

	start := Coord{x: -1}

	for scanner.Scan() {
		line := []rune(scanner.Text())
		matrix = append(matrix, line)

		if x := lo.IndexOf(line, 'S'); x != -1 {
			start.x = x
		}

		if start.x < 0 {
			start.y++
		}
	}

	dirs := lo.Filter(Dir('S'), func(d Coord, _ int) bool {
		next := start.Move(d)

		if next.x < 0 || next.y < 0 {
			return false
		}

		dirs := Dir(matrix[next.y][next.x])
		return lo.Some(dirs, []Coord{d.Op()})
	})

	if len(dirs) != 2 {
		fmt.Println(dirs)
		panic("wrong number of start directions")
	}

	pos := []Coord{start, start}

	for i := int64(1); ; i++ {
		for j, d := range dirs {
			next := pos[j].Move(d)
			nextDirs := Dir(matrix[next.y][next.x])

			found := -1
			for k, dd := range nextDirs {
				if !dd.Op().Eq(d) {
					found = k
				}
			}
			if found < 0 {
				panic("no way back")
			}

			pos[j] = next
			dirs[j] = nextDirs[found]

			if pos[0].Eq(pos[1]) {
				return i
			}
		}
	}

	return -1
}
