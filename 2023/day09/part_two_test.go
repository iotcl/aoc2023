package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartTwo(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int64
	}{
		{
			input:    `10  13  16  21  30  45`,
			expected: 5,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partTwo(reader)

			require.Equal(t, tc.expected, actual)
		})
	}
}
