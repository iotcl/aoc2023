package main

import (
	"bufio"
	"io"
	"strings"

	"github.com/samber/lo"
)

func prevInSeq(seq []int64) int64 {
	l := len(seq)

	next := lo.Reduce(seq, func(agg []int64, _ int64, i int) []int64 {
		if (i + 1) >= l {
			return agg
		}
		return append(agg, seq[i+1]-seq[i])
	}, []int64{})

	if lo.EveryBy(next, func(v int64) bool { return v == 0 }) {
		return seq[0]
	}

	return seq[0] - prevInSeq(next)
}

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()

		nums := lo.Map(strings.Fields(line), parseNum)

		sum += prevInSeq(nums)
	}

	return sum
}
