package main

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func parseNum(s string, _ int) int64 {
	n, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic("number not parsed")
	}

	return n
}

func nextInSeq(seq []int64) int64 {
	l := len(seq)

	next := lo.Reduce(seq, func(agg []int64, _ int64, i int) []int64 {
		if (i + 1) >= l {
			return agg
		}
		return append(agg, seq[i+1]-seq[i])
	}, []int64{})

	if lo.EveryBy(next, func(v int64) bool { return v == 0 }) {
		return seq[l-1]
	}

	return seq[l-1] + nextInSeq(next)
}

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()

		nums := lo.Map(strings.Fields(line), parseNum)

		sum += nextInSeq(nums)
	}

	return sum
}
