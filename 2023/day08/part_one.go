package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

func partOne(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var instr []rune
	nodes := map[string]map[rune]string{}

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			continue
		}
		if len(instr) == 0 {
			instr = []rune(line)
			continue
		}
		name, dests, ok := strings.Cut(line, " = ")
		if !ok {
			fmt.Println(line)
			panic("name not found")
		}
		left, right, ok := strings.Cut(dests, ", ")
		if !ok {
			fmt.Println(line)
			panic("destinations not found")
		}
		node := map[rune]string{}
		node['L'] = strings.Trim(left, "() ")
		node['R'] = strings.Trim(right, "() ")

		nodes[name] = node
	}

	//fmt.Println(nodes)

	var steps int64

	for loc := "AAA"; loc != "ZZZ"; steps++ {
		dir := instr[steps%int64(len(instr))]

		//fmt.Printf("Going from %s taking %c ending at %s\n", loc, dir, nodes[loc][dir])

		loc = nodes[loc][dir]
	}

	return steps
}
