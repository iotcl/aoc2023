package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	var instr []rune
	nodes := map[string]map[rune]string{}

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			continue
		}
		if len(instr) == 0 {
			instr = []rune(line)
			continue
		}
		name, dests, ok := strings.Cut(line, " = ")
		if !ok {
			fmt.Println(line)
			panic("name not found")
		}
		left, right, ok := strings.Cut(dests, ", ")
		if !ok {
			fmt.Println(line)
			panic("destinations not found")
		}
		node := map[rune]string{}
		node['L'] = strings.Trim(left, "() ")
		node['R'] = strings.Trim(right, "() ")

		nodes[name] = node
	}

	runners := lo.Filter(lo.Keys(nodes), func(name string, _ int) bool {
		return strings.HasSuffix(name, "A")
	})

	fmt.Println(runners)

	steps := lo.Map(runners, func(name string, _ int) int64 {
		var s int64
		for ; !strings.HasSuffix(name, "Z"); s++ {
			dir := instr[s%int64(len(instr))]

			name = nodes[name][dir]
		}
		return s
	})

	var gcd func(a, b int64) int64
	gcd = func(a, b int64) int64 {
		if b == 0 {
			return a
		}
		return gcd(b, a%b)
	}

	lcm := func(a, b int64) int64 {
		return (a / gcd(a, b)) * b
	}

	return lo.Reduce(steps, func(agg int64, s int64, _ int) int64 {
		return lcm(agg, s)
	}, 1)
}
