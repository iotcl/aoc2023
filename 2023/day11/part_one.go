package main

import (
	"bufio"
	"io"

	"github.com/samber/lo"
)

type Coord struct {
	x, y int
}

func distance(a, b Coord) int {
	abs := func(i int) int {
		if i < 0 {
			return i * -1
		}
		return i
	}

	return abs(a.x-b.x) + abs(a.y-b.y)
}

func partOne(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var y, width int
	var galaxies []Coord

	var emptyCols []int
	var emptyRows []int

	for scanner.Scan() {
		line := scanner.Text()
		width = len(line)

		empty := true

		lo.ForEach([]rune(line), func(c rune, x int) {
			if c != '#' {
				return
			}
			galaxies = append(galaxies, Coord{x: x, y: y})

			empty = false
		})

		if empty {
			emptyRows = append(emptyRows, y)
		}
		y++
	}

	lo.Times(width, func(x int) int {
		any := lo.SomeBy(galaxies, func(coord Coord) bool {
			return coord.x == x
		})
		if !any {
			emptyCols = append(emptyCols, x)
		}
		return 0
	})

	sum := lo.Reduce(galaxies, func(agg int, a Coord, i int) int {
		return lo.Reduce(galaxies[i+1:], func(agg int, b Coord, j int) int {
			dist := distance(a, b)

			dist += lo.CountBy(emptyRows, func(y int) bool {
				return (a.y < y && y < b.y) || (b.y < y && y < a.y)
			})

			dist += lo.CountBy(emptyCols, func(x int) bool {
				return (a.x < x && x < b.x) || (b.x < x && x < a.x)
			})

			return agg + dist
		}, agg)
	}, 0)

	return sum
}
