package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPartTwo(t *testing.T) {
	for _, tc := range []struct {
		input    string
		factor   int
		expected int
	}{
		{
			input: `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`,
			factor:   10,
			expected: 1030,
		},
		{
			input: `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`,
			factor:   100,
			expected: 8410,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			actual := partTwo(reader, tc.factor)

			require.Equal(t, tc.expected, actual)
		})
	}
}
