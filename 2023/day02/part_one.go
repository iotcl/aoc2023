package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func partOne(r io.Reader) int64 {
	maxs := map[string]int64{
		"red":   12,
		"green": 13,
		"blue":  14,
	}

	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		line := scanner.Text()

		game, rest, found := strings.Cut(line, ": ")
		if !found {
			panic("game not found")
		}
		_, nr, found := strings.Cut(game, " ")
		if !found {
			panic("game number not found")
		}
		g, err := strconv.ParseInt(nr, 10, 64)
		if err != nil {
			panic("game number not parsed")
		}

		valid := true

		for _, grab := range strings.Split(rest, "; ") {
			for _, cube := range strings.Split(grab, ", ") {
				c, color, found := strings.Cut(cube, " ")
				if !found {
					panic("cube not parsed")
				}

				cnt, err := strconv.ParseInt(c, 10, 64)
				if err != nil {
					fmt.Println(c)
					panic("cube count not parsed")
				}

				max, ok := maxs[color]
				if !ok || cnt > max {
					valid = false
					break
				}
			}
		}

		if valid {
			sum += g
		}

	}

	return sum
}
