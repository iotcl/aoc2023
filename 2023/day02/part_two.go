package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
)

func partTwo(r io.Reader) int64 {

	scanner := bufio.NewScanner(r)

	var sum int64

	for scanner.Scan() {
		mins := map[string]int64{}
		line := scanner.Text()

		_, rest, found := strings.Cut(line, ": ")
		if !found {
			panic("game not found")
		}
		// _, nr, found := strings.Cut(game, " ")
		// if !found {
		// 	panic("game number not found")
		// }
		// g, err := strconv.ParseInt(nr, 10, 64)
		// if err != nil {
		// 	panic("game number not parsed")
		// }

		for _, grab := range strings.Split(rest, "; ") {
			for _, cube := range strings.Split(grab, ", ") {
				c, color, found := strings.Cut(cube, " ")
				if !found {
					panic("cube not parsed")
				}

				cnt, err := strconv.ParseInt(c, 10, 64)
				if err != nil {
					fmt.Println(c)
					panic("cube count not parsed")
				}

				m, ok := mins[color]
				if ok {
					mins[color] = lo.Max([]int64{cnt, m})
				} else {
					mins[color] = cnt
				}
			}
		}

		prod := lo.Reduce(lo.Values(mins), func(agg int64, item int64, _ int) int64 {
			return agg * item
		}, 1)

		sum += prod
	}

	return sum
}
