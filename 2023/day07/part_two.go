package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/samber/lo"
	"golang.org/x/exp/slices"
)

func (h Hand) valueTwo() int64 {
	countMap := lo.CountValues(h.cards)

	jokers, ok := countMap[0]
	if ok {
		delete(countMap, 0)
	}
	counts := lo.Values(countMap)

	slices.SortFunc(counts, func(a, b int) bool {
		return a >= b
	})

	counts = append(counts, make([]int, 5-len(counts))...)

	val := lo.Reduce(counts, func(agg int64, c int, _ int) int64 {
		agg = agg*16 + int64(c+jokers)
		jokers = 0

		return agg
	}, 0)

	return lo.Reduce(h.cards, func(agg int64, c int64, _ int) int64 {
		return agg*16 + c
	}, val)
}

func partTwo(r io.Reader) int64 {
	scanner := bufio.NewScanner(r)

	symbols := []rune("J123456789TQKA")

	var hands []Hand

	for scanner.Scan() {
		line := scanner.Text()

		cardsStr, scoreStr, ok := strings.Cut(line, " ")
		if !ok {
			fmt.Println(line)
			panic("line invalid")
		}

		cards := lo.Map([]rune(cardsStr), func(s rune, _ int) int64 {
			return int64(lo.IndexOf(symbols, s))
		})

		score, err := strconv.ParseInt(scoreStr, 10, 64)
		if err != nil {
			fmt.Println(score)
			panic("score not parsed")
		}

		hands = append(hands, Hand{
			cards: cards,
			score: score,
		})
	}

	slices.SortFunc(hands, func(a, b Hand) bool {
		return a.valueTwo() < b.valueTwo()
	})

	return lo.Reduce(hands, func(agg int64, h Hand, i int) int64 {
		return agg + (h.score * int64(i+1))
	}, 0)
}

// should be 250665248
